import os

import sys
import re
import requests
from packaging.version import Version

print("Starting get build version script")


def current_pypi_version(
    package_name=None, repo_url=None, username=None, password=None, ignore_redirect=True
):
    print(f"Fetching current version of {package_name} from {repo_url}")

    if repo_url.lower() == "pypi":
        repo_url = "https://pypi.org"

    url = f"{repo_url}/simple/{package_name}"

    print(f"Resolved url {url}")

    try:
        if "gitlab" in repo_url.lower():
            response = requests.get(url, auth=(username, password))
        else:
            response = requests.get(url)
        if response.status_code == 200:
            if (
                ignore_redirect
                and "gitlab" in repo_url.lower()
                and "gitlab" not in response.url.lower()
            ):
                print("Ignoring Gitlab Redirect")
                # Gitlab has redirected us to pypi!!
                return None

            versions = re.compile(r"\d+(?:\.\d+)+(?=.tar)").findall(response.text)
            if versions:
                print(f"Found versions {versions}")
                versions.sort(key=Version)
                version = versions[-1]

                if version is None:
                    raise ValueError(
                        f"Error sorting version from versions {versions} at {url}, "
                        f"response {response}:{response.status_code}:{response.text}"
                    )
                return version
            else:
                raise ValueError(
                    f"Error finding version from url {url}, response "
                    f"{response}:{response.status_code}:{response.text}"
                )
        else:
            raise ValueError(
                f"Error reaching url {url}, response {response}"
                f":{response.status_code}:{response.text}"
            )

    except Exception as err:
        raise ValueError(f"Error reaching url {url}, {err}")


def increment_version(version, base="0.0.1"):
    if version is None:
        print(f"No current version, using base version {base}")
        return base
    version = version.split(".")
    version[2] = str(int(version[2]) + 1)
    new_version = ".".join(version)
    print(f"Version {version} was incremented to {new_version}")
    return new_version


if len(sys.argv) < 5:
    raise ValueError("missing argument")


version = increment_version(
    current_pypi_version(
        package_name=sys.argv[1],
        repo_url=sys.argv[2],
        username=sys.argv[3],
        password=sys.argv[4],
    )
)

with open("BUILD_VERSION", "w") as f:
    f.write(version)
