import sys


def increment_version(version, base="0.0.1"):
    if version is None:
        print(f"No current version, using base version {base}")
        return base
    split_version = version.split(".")
    split_version[2] = str(int(split_version[2]) + 1)
    new_version = ".".join(split_version)
    print(f"Version {version} was incremented to {new_version}")
    return new_version


with open("RELEASE_VERSION", "w") as f:
    f.write(increment_version(sys.argv[1] if len(sys.argv) >= 2 else None))
