import os
import inspect
import shutil

from graphql_service_framework.schema_tracker import SchemaTracker
from graphql_service_framework.utils import increment_version, install_package


def get_directory_list(path):
    dirs = []

    if os.path.isfile(path):
        return []

    if len([f for f in os.listdir(path) if f.endswith('.py') and f != "schema_utils.py"]):
        dirs.append(path)
    else:
        for d in os.listdir(path):
            new_path = os.path.join(path, d)
            if os.path.isdir(new_path):
                dirs += get_directory_list(new_path)

    return dirs


def scan_and_build_schema_packages():
    print(f"Moving schemas to the root directory.")
    for d in get_directory_list("./"):
        if d != "./":
            print(f"Moving {d} to the root directory.")
            shutil.move(d, "./")

    commit_ref_name = os.getenv("CI_COMMIT_REF_NAME", "")
    commit_short_sha = os.getenv("CI_COMMIT_SHORT_SHA", "")

    username = os.getenv("SCHEMAS_PYPI_USERNAME", "")
    username = os.getenv("SCHEMAS_PYPI_USERNAME", "")
    token = os.getenv("SCHEMAS_PYPI_PASSWORD", "")
    repository = os.getenv("SCHEMAS_PYPI_REPOSITORY", "").lower().replace("https://", "")
    index_url = f"https://{username}:{token}@{repository}/simple"
    schemas_package_name = "schemas"
    suffix = "schema"
    base_version = "0.0.1"

    print(f"{schemas_package_name} build in branch {commit_ref_name} commit {commit_short_sha}.")
    developmental_release = commit_ref_name if commit_ref_name.startswith("dev") else None

    dev_message = "developmental release " + developmental_release + " " if developmental_release else ""
    print(f"Deploying {dev_message}{schemas_package_name} to {repository}.")

    root_directory = os.path.dirname(os.path.realpath(__file__))
    release_setup = os.path.join(root_directory, "schemas_setup.py")
    release_version = os.path.join(root_directory, "release_version.txt")
    release_notes = os.path.join(root_directory, "release_notes.md")

    schema_updates = SchemaTracker.find_schemas(
        root_directory, schemas_package_name, index_url, suffix
    )

    major = sum(s.major_changes_count() for s in schema_updates)
    minor = sum(s.minor_changes_count() for s in schema_updates)
    micro = sum(s.micro_changes_count() for s in schema_updates)

    major_s = sum(bool(s.major_changes_count()) for s in schema_updates)
    minor_s = sum(bool(s.minor_changes_count()) for s in schema_updates)
    micro_s = sum(bool(s.micro_changes_count()) for s in schema_updates)

    for schema_update in schema_updates:
        schema_update.release(developmental_release=developmental_release)

    release = None

    if install_report := install_package(schemas_package_name, index_url):
        previous_version = install_report["install"][0]["metadata"]["version"]
        print(f"Found a previous release - version {previous_version}")
        if major:
            release = increment_version(previous_version, 0)
        elif minor:
            release = increment_version(previous_version, 1)
        elif micro:
            release = increment_version(previous_version, 2)
    else:
        print(f"Unable to find a previous `{schemas_package_name}` release.")
        release = base_version

    if release:
        change_log = (
            f"# *{schemas_package_name.capitalize()}* Release {release}" f" Summary\n"
        )
        if major or major_s:
            change_log += f"{major} breaking change{'' if major == 1 else 's'} in {major_s} schema{'' if major_s == 1 else 's'}.\n"
        else:
            change_log += "No breaking changes.\n"
        if minor or minor_s:
            change_log += f"{minor} safe change{'' if minor == 1 else 's'} in {minor_s} schema{'' if minor_s == 1 else 's'}.\n"
        if micro or micro_s:
            change_log += f"{micro} micro change{'' if micro == 1 else 's'} in {micro_s} schema{'' if micro_s == 1 else 's'}.\n"
        change_log += "\n## Released versions:\n"

        requires = []
        for schema_update in schema_updates:
            schema_update.release(developmental_release=developmental_release)
            required_version = schema_update.generate_new_version(developmental_release=developmental_release).rsplit(".", 1)[0]
            requires.append(f"        '{schema_update.package_name()} ~= {required_version}'")
            change_log += schema_update.change_log(developmental_release=developmental_release)

        if developmental_release:
            print(f"Skipping main package release, {dev_message}")

        else:
            print(f"Creating new release in {release_setup}, notes {release_notes}")

            setup_data = (
                inspect.cleandoc(
                    """
                    from setuptools import setup

                    setup(
                        name='SERVICE_NAME',
                        version='SERVICE_VERSION',
                        install_requires=[
                            INSTALL_REQUIRES
                        ],
                        packages=[]
                    )
                    """
                )
                .replace("SERVICE_NAME", schemas_package_name)
                .replace("SERVICE_VERSION", release)
                .replace("INSTALL_REQUIRES", "# schemas\n" + ",\n".join(requires))
            )

            with open(release_setup, "w") as file:
                file.write(setup_data)

            with open(release_notes, "w") as file:
                file.write(change_log)

            with open(release_version, "w") as file:
                file.write(release)

            print(change_log)

    else:
        print("No release needed.")


if __name__ == '__main__': 
    scan_and_build_schema_packages()
